# weighing
雲端電子秤整合系統
```
UI Framework by Buefy (https://buefy.org/ + https://bulma.io/documentation/)
Utility-first CSS framework by Tailwindcss (https://v2.tailwindcss.com/docs)
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
