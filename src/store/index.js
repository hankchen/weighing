// vue basic lib
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import axios from 'axios'

Vue.use(Vuex)
Vue.use(VueCookies)

// 客製/第三方 lib
// import md5 from 'md5'
// import axios from 'axios'
import fetchData from 'src/libs/fetchData.js'
import router1 from 'src/router'

import { deviseFunction, ensureDeviseSynced, deviseSetLoginInfo } from '../libs/deviseHelper.js'

// import { showAlert, setLoading } from '../libs/appHelper.js'
import { showAlert } from '../libs/appHelper.js'
import { S_Obj,L_Obj,IsMobile, EncodeMe } from 'src/fun1.js'	// 共用fun在此!

let IsTest	= false
	,AccData	= L_Obj.read('userData') || {}
	,HoHttp		= AccData.HoHttp || 'https://8001.jh8.tw'

// 依據環境參數，來決定 API baseUrl 的值
const baseUrl = '8012.jh8.tw'
	,Test8012 	= '8012test.jh8.tw'
	,IsDev 			= S_Obj.isDebug 	//開發debug
	,IsHank 		= /hank/.test(AccData.UserCode);	// @@test

const state = {
  appSite: 'weighing', //'kakar', // 系統參數: 站台
  promise: null, // 保存promise
  // 是否在殼裡面(判斷能否使用殼相關接口的依據)
  isDeviseApp: false,
  // 啟動/關閉 『讀取中』的動畫效果
  isLoading: false,
  loadingMsg: '',
  // 顯示手動輸入 QRcode 表格
  showInputQRCode: false,
  // 客製的彈窗 (用於 alert / confirm)
  customModal: {
    type: '', text: '', confirmText: '', cancelText: '',
    // confirm note
    confirmNoteTitle: '', // 附註標題
    confirmNoteChoice: [], // 附註選項
    confirmNoteChoosed: '', // 附註選項已選
    confirmNoteRemark: '', // 附註選了其他要手key的說明
    confirmNoteRemarkPlaceHolder: '', // 其他原因的place holder
    confirmNoteWarnMsg: '', // 警告訊息
    confirmNoteText: '', // 附註標題(style2)
  },
  // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
  // true => touch,
  // false => auto (幾乎只用於首頁)
  cssTouch: true,
	searchURL:{},
  isPageName: '', //內頁名稱
  // API url
  api: {
    main: `https://${baseUrl}/public/AppData.ashx`,
    Url8012: `https://${Test8012}/public/AppDataVIP.ashx`,
    figUrl: `https://${baseUrl}/public/AppDataVIPOneEntry.ashx`,
    // OneEntry 主服務
    appNcw: `https://${baseUrl}/Public/AppNCWOneEntry.ashx`,
    // 企業的公開資料
    publicUrl: `https://${baseUrl}/public/newsocket.ashx`,
    // 企業的會員資料
    memberUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,	// +token
    // 預約資料
    bookUrl: `https://${baseUrl}/public/AppBooking.ashx`,
    // 預約的token資料
    tokenUrl: `https://${baseUrl}/public/AppFamilyGourmet.ashx`,
    // 圖片的路徑
    picUrl: `https://${baseUrl}`,
  },
  // 基本資訊
  baseInfo: {
    // 英文常稱
    isFrom: 'weighingapp',
    // 企業號
    EnterpriseID: 'jinher',
    // FunctionID
    FunctionID: '450801', // 碳佐functionid=450801
    // 是否接收推播訊息 (跟殼要)
    pushNotify: '1', // 預設要開啟, 1:開啟推播, 0:關閉推播
    // GPS 資訊 (跟殼要)
    gps: {},
    // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
    brightness: '120',
    // 前端打包檔版本號 (跟殼要)
    WebVer: '',
    // 殼的裝置 (iOS / Android)
    AppOS: '',
    // 殼是否有新版本
    DeviseVersionIsDiff: false,
  },
	// 會員登入頁的表單
	loginForm: { account: '', password: '' },
  // 簡訊認證設定 (需要從殼取)
  SMS_Config: {
    // 簡訊回傳的驗證碼
    reCode: "",
    // 每天最多簡訊次數 (預設為 5 次)
    APPSMSDayTimes: '5',
    // 每封簡訊發送間隔 (預設為 50 秒)
    AppSMSIntervalMin: '50',
    // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
    SMSSendQuota: '',
  },
  // 會員接口相關資訊
  member: {
    mac: AccData.mac,
    Tokenkey: AccData.tokenkey,
    code: AccData.UserCode,
  },
  // 會員帳號資訊
  userData: AccData,
  // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠券 ... etc)
	// memberData: {
		// // 會員卡
		// card_info: {},
		// // 會員資料
		// vip_info: {},
		// // 會員條碼 QRcode
		// qrCode: { image: '', value: '', loaded: false, },
		// // 會員儲值交易資料(使用紀錄)
		// depositRecord: [],
	// },
  // 企業公開資料相關
  publicData: {
    // LOGO 圖檔
    logo: '',
    logoLoaded: false,
    // DB connect 位置
    connect: '9001',
    // 首頁
    index: {
      // 主畫面可滑動的 Banners
      slideBanners: [],
      // 固定的副 Banner
      subBanner: [],
    },
    // 品牌資訊(關於我們, 使用者條款, 常見問題 ... etc)
    brandInfo: [],
    // 門店資訊頁
    storeList: { store: [] },

    appProducer: [], // 生產者
    appDeliver: [], // 運銷商
    appFoods: [], // 農產品
    weight: 0, 		// 磅秤數值

  },

  // 卡片條碼 QRcode
  cardQRCode: { image: '', value: '', loaded: false },
  cardListScroll2: 0,
  fontSizeBase: 1
}

const mutations = {
  /** 暫存字體大小 */
  setFontSizeTemp(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
  },
  /** 永久保存字體大小 */
  setFontSizeForever(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
    // 存入cookie
    VueCookies.set('fontSize', floatFontSize)
    // 若在殼中 => 存入殼裡
    // if (IsMobile()) {
      // let setThing = `{"spName":"fontSize", "spValue": ${ floatFontSize }}`
      // deviseFunction('SetSP', setThing, '')
      // // console.log('===> 已存入殼裡 =>' + setThing)
    // }
  },
  // 存入系統參數：站台 & 基礎參數
  setAppSite(state, site) {
    state.appSite = site
  },
  // 設定 cssTouch
  setCssTouch(state, status){ state.cssTouch = status },
  // 設定是否在殼裡面
  setIsDeviseApp(state, status) { state.isDeviseApp = status },
  // 『讀取中...』 切換
  setLoading(state, data) {
    state.isLoading = data.status
	/* 追加顯示-載入中訊息 */
	var msg = ''
	switch (data.mType) {
	case 1:
		msg = '資料處理中，請稍候...'
		break;
	case 2:
		msg = '資料加載中，請稍候...'
		break;
	case 3:
		msg = '急速加載中...'
		break;
	case 4:
		msg = '努力搶券中，請稍候...'
		break;
	default:
	}
	state.loadingMsg = msg
  },
  // 『顯示手動輸入 QRcode 表格』切換
  setInputQRCode(state, status) { state.showInputQRCode = status },
  // 設定彈跳視窗的資料 (顯示 / 隱藏)
  setCustomModal(state, { type, text, cancelText, confirmText,
    confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
    confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText,
    resolve, reject }) {
    cancelText = cancelText || '取消'
    confirmText = confirmText || '確認'

    state.customModal = { type, text, cancelText, confirmText,
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText}

    if (resolve && reject) state.promise = { resolve, reject }
  },
  // 存入手機簡訊認證的設定
  setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
  // 設定簡訊驗證碼
  setSMSreCode(state, data) { state.SMS_Config.reCode = data },
  // 存入基本資訊
  setBaseInfo(state, data) { state.baseInfo = Object.assign({}, state.baseInfo, data) },
  // 更新會員資訊
  setUserData(state, data) { state.userData = data },
  // 存入Mac (在沒有登入的狀態)
  setMacNoLogin(state, mac) {
    // 存入 store
    state.member.mac = mac
  },
  // 存入會員登入資訊
	setLoginInfo(state, payload) {
		const data = payload.data

		// 存入 store
		state.userData = data
		state.member.mac = data.mac
		state.member.code = data.UserCode
	},
  // 清除企業 app 的會員資料
  // clearAppMemberData(state) { state.memberData = rawRemberData() },

  // 會員登出
	setLogout(state) {

		state.userData = {}
		// 清除會員基本資訊
		state.member.mac = ''
		state.member.code = ''
	},

  // 存入企業 app 的 LOGO 資訊
  setAppLogo(state, data) { state.publicData.logoLoaded = true ;state.publicData.logo = data },
  // 存入企業 app 的 db connect 資訊
  setAppConnect(state, data) { const connect = data || '9001' ; state.publicData.connect = connect },
  // 存入首頁主畫面可滑動的 Banners - 全餐
  setAdvertise(state, data) {
    // console.log('setAdvertise ===> data =>', data)
    // === only for test === start
    // data = {"ADBanner":[],"SUBanner":[]}
    // === only for test === end
    state.baseInfo.publicData.advertise.adData = data
  },
  setCompanyData(state, data) { state.baseInfo.publicData.companyData = data },
  // 存入首頁主畫面可滑動的 Banners
  setIndexSlideBannersWuhu(state, data) { state.baseInfo.publicData.index.slideBanners = data },
  // 存入首頁固定的副 Banner
  setIndexSubBanner(state, data) { state.publicData.index.subBanner = data },

  // 存入條碼 QRcode
	setShowQrcode(state, data) { state.ShowQrcodeFn = data },
  // 存入殼的版本是否需要更新
  SetDeviseVersionIsDiff(state, status) {
    const value = (status === '1') ? true : false
    state.baseInfo.DeviseVersionIsDiff = value
  },
  // 儲存(從殼過來的) 磅秤數值
  setGetweighing(state, value) {
		let v1 = value.replace(/kg/g, '');
		state.publicData.weight = parseFloat(v1) || 0;
  },

  /* 存入 生產者 */
  setProducer(state, data) {
    state.publicData.appProducer = data || [];
  },
  /* 存入 運銷商 */
  setDeliver(state, data) {
    state.publicData.appDeliver = data || [];
  },
  /* 存入 生產者x農產品 */
  setFoods(state, data) {
    state.publicData.appFoods = data || [];
  },
  /* 存入 集貨站 */
  setStations(state, data) {
    state.publicData.appStations = data || [];
  },

}

const getters = {
  fontSizeBase: state => {
    let fontSize = state.fontSizeBase
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    return parseFloat(fontSize)
  },
  // 站台 router
  router: () => {//state => {
    // if (state.appSite === 'wuhulife') return routerWuhulife
    return router1
  },
  // 是否登入
  // isLogin: state => {
  // isLogin: () => {
		/** @Fix: 有時序問題 */
    // return !!(state.member.code && state.member.mac && state.member.Tokenkey);
    // return !!(AccData.UserCode && AccData.tokenkey);
  // },
	requestHead: () => {
		return { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
	},
  // 的 requestBody
  kakarBody: state => {
    const {mac, code} = state.member
    const {isFrom, EnterpriseID, FunctionID} = state.baseInfo
    return { FunctionID, EnterpriseID, isFrom, mac, Account: code, }
  },
  // 企業 app 的 requestBody
  appBody: state => (body) => {
    const inputBody = ( body || {} )
    const {mac} = state.member
    let {isFrom, EnterpriseID} = state.baseInfo
		const epid = state.userData.EnterPriseID
		if (epid) {
			EnterpriseID = epid;
		} else {
			if (state.member.mac) {
				throw 'No EnterpriseID!!';
			}
		}

    const basicBody = {
      mac, isFrom, EnterpriseID,
      // EnterpriseID: EnterPriseID,
      Account: state.member.code,
    }
    return Object.assign({}, basicBody, inputBody)
  },
  connect: (state) => {
    return state.publicData.connect
  },
  // 取手機的螢幕亮度值
  deviseBrightness: state => {
    if (state.baseInfo.brightness <= 70) return 120
    return state.baseInfo.brightness
  },
}

const actions = {
  /* 資料相關 */
  // init 時執行所需的 action
  async fetchInitData() {
    // 1. 跟殼取資料
    await ensureDeviseSynced()
  },
  async ensureGetDeviseGPS({/*state,*/ commit}) {
    // console.log('ensureGetDeviseGPS-IsMobile: ', IsMobile());	// @@
    if (IsMobile()) {
      // (在殼裡面) call 跟殼取 GPS 資訊的 API 與 callback function
      deviseFunction('gps', '', '"cbFnSyncDeviseGPS"')
    } else {
      // 不在殼裡面
      if ("geolocation" in navigator) {
        /* geolocation is available */
        navigator.geolocation.getCurrentPosition((position)=>{
          //console.log('navigator ===> position =>', position)
          // 轉成 google maps api 需要的預設格式
          const gpsData = {
						gps: {
							lat: position.coords.latitude,		// 緯度
							lng: position.coords.longitude		// 經度
						}
          }
          // 將 gps 存入 state.baseInfo
          commit('setBaseInfo', gpsData)
        }, (err)=>{
          console.warn('navigator-error: ', err)
        })
      } else {
        /* geolocation IS NOT available */
      }
    }
  },

  async fetchProducer({getters, commit}) {
    const data = await Get950201Body(getters, 'M_Scales_Producer')
    data && commit('setProducer', data)
  },
  async fetchDeliver({getters, commit}) {
    const data = await Get950201Body(getters, 'M_Scales_Deliver')
    data && commit('setDeliver', data)
  },
  async fetchFoods({getters, commit}) {
    const data = await Get950201Body(getters, 'M_Scales_Producer_Food')
    data && commit('setFoods', data)
  },
  async fetchStations({getters, commit}) {
    const data = await Get950201Body(getters, 'M_Scales_Station')
// IsDev && console.log('fetchStations-data: ', data);	// @@
    data && commit('setStations', data)
  },
	/* 寫入秤重記錄 */
  async ensureScaleWeight({getters}, payload){
		const param1 = {
			FoodID: payload.FoodID,
			FoodName: payload.FoodName,
			ProducerID: payload.ProducerID,
			Unit: payload.Unit,
			DeliverID: payload.DeliverID,
			Weight: payload.Weight,
			UserCode: payload.UserCode,
			UnitPrice: payload.UnitPrice,		// 單價
    };
    const data = await Get950201Body(getters, 'M_Scales_Weight', param1)
    return new Promise(resolve => resolve(data))
  },
	/* gid抓/作廢-秤重記錄(掃碼 */
  async GetSetScaleGid({getters}, payload) {
		let bWrite 	= 0
			,gid2			= ''
			,lotNo		= ''

		if (payload) {
			bWrite = 1
			gid2 = payload.GID
			// lotNo = payload.LotNo
		} else {
			gid2 = S_Obj.read('M_Scales_GID');
			lotNo = S_Obj.read('M_Scales_LotNo');
		}

		const param1 = {
			GID: gid2,
			LotNo: lotNo,
			DeleFlag: bWrite
		};

    const data = await Get950201Body(getters, 'M_Scales_DeleFlag', param1)

IsHank && alert('掃碼_秤重記錄-param1: '+JSON.stringify(param1)+'\ndata: \n'+JSON.stringify(data));	// @@

		// console.warn('GetSetScaleGid-data: ', data);	// @@
		if (!bWrite) {	// 掃碼
			if (data.length) {
				S_Obj.save('M_SCALES_ONE', data[0]);
				location.hash = '#/weigscan'

			} else {
				showAlert("找不到記錄: "+lotNo);
			}
		// } else {		// 作廢
		}

  },
  /* 設秤重記錄-備註描述 */
  updateScaleRemark({getters}, payload){
		Get950201Body(getters, 'M_Scales_Weight_Remark', payload);

		// // const {GID, Remark} = payload
		// // const param1 = {
			// // act: "M_Scales_Weight_Remark",
			// // GID: GID,
			// // Remark: Remark
    // // };
    // const data = await Get950201Body(getters, 'M_Scales_Weight_Remark', payload)
    // return new Promise(resolve => resolve(data))
  },
	/* 讀取秤重記錄-日報 */
  async ensureWeightList1({getters}, payload){
    const data = await Get950201Body(getters, 'M_Scales_Weight_List1', payload)
    return new Promise(resolve => resolve(data))
    // return new Promise(resolve => resolve(data || []))
  },
	/* 讀取秤重記錄-日報 */
  async ensureWeightCountList1({getters}, payload){
    const data = await Get950201Body(getters, 'M_Scales_Weight_List1_Count', payload)
    return new Promise(resolve => resolve(data))
  },
	/* 讀取秤重記錄-月報 */
  async ensureWeightList2({getters}, payload){
    const data = await Get950201Body(getters, 'M_Scales_Weight_List2', payload)
    return new Promise(resolve => resolve(data))
    // return new Promise(resolve => resolve(data || []))
  },
	/* 讀取秤重記錄-月報 */
  async ensureWeightCountList2({getters}, payload){
    const data = await Get950201Body(getters, 'M_Scales_Weight_List2_Count', payload)
    return new Promise(resolve => resolve(data))
  },


  /* 會員登入 */
  async ensureLogin({state, getters, commit}, formData) {
    // setLoading(true);
    const {isFrom} = state.baseInfo
    // const pwd = md5(formData.password)
    const pwd 	= formData.password
			,account 	= formData.account
		let url = state.api.appNcw
			,body = {
      isFrom,
			"act": "login",
      "EnterpriseID": formData.enterprise,
      "userphone": account,
      "userPwd": pwd,
    }
		const res = await axios.post(url, body, getters.requestHead)
			,isOK		= res.data.ErrorCode == 0
			,data		= isOK ? JSON.parse(res.data.Remark) : {}
    // const data = await fetchData({url, body, head})
		IsDev && console.log('memberLogin-data: ', data);	// @@
		if (isOK) {
			IsTest = account.substring(0,3) == 'jh8'
			HoHttp = IsTest ? 'https://'+Test8012 : res.data.HoHttp;
			data.HoHttp = HoHttp;
			AccData = data;
			L_Obj.save('userData', data);
			L_Obj.save('loginForm', formData);


			// => 存入會員資訊 (cookie, store)
			commit('setLoginInfo', {data: data, pwd: pwd})
			// => 存入會員資訊 (殼)
			deviseSetLoginInfo({type: 'set', data: data, pwd: pwd})
    }
    return new Promise(resolve => resolve(res.data))
  },

  //  會員登出
  memberLogout({commit, getters}, bManual) {
    // 清除 殼 裡的會員資料
    deviseSetLoginInfo({type: 'reset'})
    // 清除 state 裡的會員資料
    commit('setLogout');

		/** @PS: 殼會固定先清空,改手按才調用 */
		bManual && L_Obj.del('userData');	

    // 導向登入頁
    getters.router.push('/')
  },

  //  取 cardQRcode
  async fetchCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )
    const url = state.api.memberUrl
    const body = getters.appBody({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (公開) 取主畫面可滑動的 Banners 資料
  // async fetchIndexSlideBannersWuhu({commit}, payload){
		// let data = await GetJSON_Banner(payload, 'M')
    // if (data.error) return
    // commit('setIndexSlideBannersWuhu', data)
  // },
  // // (公開) 取固定的副 Banner 資料
  // async fetchIndexSubBannerWuhu({commit}, payload){
		// let data = await GetJSON_Banner(payload, 'D')

    // if (data.error) return
		// /** @@Test 副Banner資料 */
		// // data = await GetJsonTest('subBanner')	// @@
		// // console.log('副Banner資料-item: ', data);	// @@
    // commit('setIndexSubBannerWuhu', data)
  // },

  async fetchAppConfig({state, commit},str_keys) {
    const url = `https://${baseUrl}/public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${state.baseInfo.EnterpriseID}&AppSysName=${str_keys}`
      ,body   ={}
      ,data 	= await fetchData({url, body})

    if (data && typeof data == 'string' && data.indexOf('OK,') == 0){
      const arr = data.split('OK,');
      if (arr.length > 1){
        const p_txt = `{${arr[1]}}`.replace(/'/g, "\"")
        const p_config = JSON.parse(p_txt);
        //str_keys有可能為 foodMarketShopID,xxx,yyy...不能直接p_config[str_keys]
        commit('setMarketShopID', p_config.foodMarketShopID)
      }
    }
  },

}

/* comm fun共用區 */
// async function Get950201Body(getters, _act, para1) {
async function Get950201Body(getters, _act, para1) {
	let param7 = {act: _act,FunctionID:"950201"}
	if (para1) {
		if (para1.orderby) {
			param7.orderby = para1.orderby;
			delete para1.orderby;
		}
		if (para1.srow) {
			param7.srow = para1.srow;
			delete para1.srow;
		}
		if (para1.erow) {
			param7.erow = para1.erow;
			delete para1.erow;
		}
		param7.param = EncodeMe(para1);
	}
	let body = getters.appBody(param7)

	const url = GetCUrl(state.api.main)
		,data 	= await fetchData({url, body})
	// return data.error ? null : data
	return data
}

function GetCUrl(curUrl) {
	const npos  = curUrl.lastIndexOf("/public")
		,_code = (npos != -1) ? curUrl.substring(npos) : ''
		,_url		= HoHttp+_code
	// console.log('GetCUrl-_url: ', _url);	// @@
	return _url;
}

export default new Vuex.Store({ state, mutations, getters, actions })
