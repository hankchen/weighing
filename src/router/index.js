import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
		path: "/",					name: "Login",
		component: () => import("../views/Login.vue"),
	}, {
		path: "/index",			name: "Home",
		component: () => import("../views/Home.vue"),
	}, {
		path: "/weigscan",	name: "WeigScan",
		component: () => import("../views/WeigScan.vue"),
	}, {
		path: "/weighing",	name: "Weighing",
		component: () => import("../views/Weighing.vue"),
	}, {
		path: "/setting",		name: "Setting",
		component: () => import("../views/Setting.vue"),
	}, {
		path: "/statistic",		name: "Statistic",
		component: () => import("../views/Statistic.vue"),
	},
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

// fix相同的路由click出錯
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}
