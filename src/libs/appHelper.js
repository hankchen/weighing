/* app 通用 helper  */

// import Vue from 'vue'
// import VueCompositionApi from '@vue/composition-api'
// Vue.use(VueCompositionApi)

import store from '../store'
import { DialogProgrammatic as Dialog } from 'buefy'

function setLoading(status, mType) {store.commit('setLoading', {status: status, mType: mType})}

// 顯示自訂的 alert 畫面
function showAlert(text) { Dialog.alert(text); }
function hideModal() { store.commit('setCustomModal', { type: '', text: '' }) }

// 顯示自訂的 confirm 畫面
// text: 大標題
// confirmText: 確認按鈕label
// cancelText: 取消按鈕label
// confirmNoteTitle: 附註標題
// confirmNoteChoice: 附註選項
// confirmNoteChoosed: 附註選項已選
// confirmNoteRemark: 附註選了其他要手key的說明
// confirmNoteRemarkPlaceHolder: 其他原因的place holder
// confirmNoteWarnMsg: 警告訊息
function showConfirm(text, confirmText, cancelText,
  confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
  confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText) {
  return new Promise((resolve, reject) => {
    store.commit("setCustomModal", {
      type: 'confirm',
      text, confirmText, cancelText,
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText,
      resolve, reject
    });
  });
}

function checkReceiver(vip) {
  var errMsg = "";
  const twMobilePhoneRegxp = /^09[0-9]{2}[0-9]{6}$/; // 台灣手機號碼格式需為09XXXXXXXX
  const cnMobilePhoneRegxp = /^1[0-9]{2}[0-9]{8}$/;  // 中國手機號碼格式需為1XXXXXXXXXX
  if (twMobilePhoneRegxp.test(vip.mobileTmp) === false && cnMobilePhoneRegxp.test(vip.mobileTmp) === false) errMsg = '手機號碼格式錯誤'
  if (vip.addrsTmp == "" || vip.receiverTmp == "") errMsg = (errMsg != ""? errMsg + '<br>收件人 或 地址未填寫':'收件人 或 收件人地址未填')
  return errMsg;
}

// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImage(imageUrl) {
    // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
  // const isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl.toLowerCase())
  // 若不是圖檔，回傳 noImageUrl
  // if (isImage === false) return require('@/assets/images/img_init.png')

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.getters.srcUrl + imageUrl
}

// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImgSubsidiary(imageUrl) {

  // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
  const isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl)
  // 若不是圖檔，回傳 notImage 字串
  if (isImage === false) return 'https://web.jh8.tw/kakar2/img/img_init.gif'

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  //console.log("qqq>>>>",store.getters.subSrcUrl + imageUrl);
  return store.getters.subSrcUrl + imageUrl
}

export { showAlert, showConfirm, setLoading, hideModal, showImage, showImgSubsidiary, checkReceiver }
