/* eslint-disable */
/* 與殼溝通相關的 function */

import axios from 'axios'
import store from 'src/store'
import { S_Obj, L_Obj} from 'src/fun1.js'	// 共用fun在此!

// import router from 'src/view/router'
import { showAlert } from 'src/libs/appHelper.js'

// const IsWebProd		= S_Obj.isWebProd
	// ,IsStaging 			= S_Obj.isStaging
const IsDev 					= S_Obj.isDebug 	//開發debug
	// ,KEY_CONFIG			= 'AppSMSIntervalMin,APPSMSDayTimes,AppSMSRegCodeLeng,foodMarketShopID'

const Ls1	= L_Obj.read('userData') || {}
	,IsHank = /hank/.test(Ls1.UserCode)		// @@test
	// ,IsTest = /jh8/.test(Ls1.UserCode)		// @@test

// IsHank && console.log('deviseHelper-IsDev: '+IsHank, IsDev);	// @@


// 打 logger
const deviseLogger = function(act, params, cbFn) {
  // 時間戳記
  const getTimeStamp = function(t) {
    // 當下時間
    const time = t || new Date()
    // 補齊位數 => e.g. 1 -> 01 ,  51 -> 051
    const pad = function(num, maxLength) {
      // 要補齊的字數
      const diffLength = maxLength - num.toString().length
      return (new Array(diffLength + 1)).join('0') + num
    }
    // => sample: " @ 13:02:02.993"  (時:分:秒.毫秒)
    return  "@ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3));
  }

  console.groupCollapsed(`devise: ${act} ${getTimeStamp()}`);
  // console.log('%c act',    'color: #afaeae; font-weight: bold', act);
  // console.log('%c params', 'color: #9E9E9E; font-weight: bold', params);
  // console.log('%c cbFn',   'color: #03A9F4; font-weight: bold', cbFn);
  console.groupEnd();
}

// 執行與殼溝通 function 的統一接口
const deviseFunction = function(act, params='', cbFn=''){
  if (!act) return console.log('deviseFunction: 請輸入 act')
  // 打 logger
  deviseLogger(act, params, cbFn)
  // 必須在殼裡面才能執行
  if (typeof(JSInterface) === 'undefined') return
  // 執行跟殼溝通的 function
  JSInterface.CallJsInterFace(act, params, cbFn)
}

// (藉由殼 API) 發送手機驗證簡訊
function sendDeviseSMSreCode(payload) {
  // payload sample: { phone: '0955111222;;886', SMSSendQuota: '20190514-3' }
  // 將已發送扣打紀錄存入殼裡
  deviseFunction('SetSP', `{"spName":"SMSSendQuota", "spValue":"${payload.SMSSendQuota}"}`, '')
  // 發送手機驗證簡訊
  deviseFunction('AppSMSReCode', payload.phone, '"cbFnSyncDeviseSMSreCode"')
}

function sendDeviseToPrn(payload) {
	const val2 = `[{"OrderID":"${payload.LotNo}","HMmain":{"PCName":"${payload.PCName}","SaleOrderNo":"${payload.LotNo}","PCDate":"${payload.PCDate}","PExpress":"${payload.PExpress}","PMarker":"${payload.PMarker}","PName":"${payload.PName}","PNameWT":"${payload.Weight} ${payload.Unit}","QRCODE":"${payload.QRCODE}"}}]`;
	// console.log('sendDeviseToPrn-val2: ', val2);	// @@
	deviseFunction('sendToPrn', val2, '"cbFnsendToPrn"');
}


// 與殼溝通，存取必要的資料
function ensureDeviseSynced() {
  // 判斷是否在殼裡面
  const isDeviseApp = store.state.isDeviseApp
  return new Promise(resolve => {
		if (isDeviseApp) {
			// 清空舊的 setSP 資料
			deviseFunction('SetSP', `{"spName":"vipApps", "spValue":""}`, '')

			// 如果在殼裡
			//   1. 跟殼取 publicJWT, connect, isFrom, EnterpriseID, 會員登入資訊, 更新 store.state
			//   2. 跟殼取微管雲的系統參數: 每天最多簡訊次數, 每封簡訊發送間隔
			//   3. (以上都做完以後) => resolve('ok') // 繼續 fetchInitData
			deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
		}

		// const _act1 = 'GetAppSysConfig'
			// ,data1		= S_Obj.read(_act1)
		// let isTrue = true

		// if (data1) {
			// store.commit('setSMS_Config', data1)
		// } else {
			// isTrue = fetchAppSysConfig();
		// }

		// if (isDeviseApp && !isTrue) {
			// deviseFunction(_act1, KEY_CONFIG, '"cbFbSaveAppSysConfig"')
		// }

    resolve('ok')
  })
}

/* 依需打api問-微管雲設定 */
// async function fetchAppSysConfig() {
	// const EID	= store.state.baseInfo.EnterpriseID
		// ,sett1	= KEY_CONFIG
		// ,host1	= 8012+(IsWebProd||IsDev ? '' : 'test')+'.jh8.tw'
		// // ,host1	= 8012+(IsWebProd ? '' : 'test')+'.jh8.tw'
		// // ,url		= `https://8012.jh8.tw/Public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${EID}&AppSysName=${sett1}`
		// ,url		= `https://${host1}/Public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${EID}&AppSysName=${sett1}`
		// ,res1 	= await axios.get(url).catch (() => {})
		// ,res2		= res1.data
		// ,hasIt 	= /OK,/.test(res2)
	// let bOK		= false;

	// /** @PS: 正常抓到時無 ErrorCode/ErrorMsg 等屬性 */
	// if (hasIt) {
		// const txt1 = res2.replace(/OK,/, '').replace(/'/g, '"')
		// IsDev && console.log('fetchAppSysConfig-txt1: ', txt1);	// @@

		// if (txt1.length) {
			// const obj1 = JSON.parse(`{${txt1}}`)
			// // console.warn('fetchAppSysConfig-obj1: ', obj1);	// @@
			// store.commit('setSMS_Config', obj1)

			// S_Obj.save('GetAppSysConfig', obj1);

			// bOK = true;
		// }
	// } else {
		// console.error('Error-fetchAppSysConfig: ', res2.ErrorMsg);
	// }

	// return bOK;
// }


// callback function: 儲存(從殼過來的) 基本設定資料
const cbFnSyncDeviseInfoToStore = function(e) {
  console.log('cbFnSyncDeviseInfoToStore ===> e=>', e)

  const getOVal = function(v1) {typeof(v1) === 'object' ? v1 : (v1 ? JSON.parse(v1) : null)};
  // const hasVal = function(v1) {return v1 && typeof(v1) != 'undefined'};

  // 過濾，只取需要的資訊
  let baseInfoData = {}
  baseInfoData.pushNotify   = e.pushNotify // 是否推播訊息
  baseInfoData.WebVer       = e.WebVer     // 前端打包檔版本
  baseInfoData.brightness   = e.brightness // 螢幕亮度值
  baseInfoData.AppOS        = e.AppOS      // 殼的裝置 (iOS / Android)
  // baseInfoData.currentEnterPriseID = e.currentEnterPriseID // 導向企業號
	// store.state.baseInfo.EnterpriseID
  // 將資訊存入 state.baseInfo
  store.commit('setBaseInfo', baseInfoData)
  // 存入點擊紀錄
	const clicks = getOVal(e.appClicks);
	clicks && store.commit('saveAppClicks', clicks)

	let userData = L_Obj.read('userData') || {}
// IsHank && alert('有已登入: \n'+JSON.stringify(userData));	// @@

  if (!userData.tokenkey) {
		// IsTest && alert('登入的資訊: \n'+JSON.stringify(e));	// @@
		userData = {
			Email: e.Email,
			EngName: e.EngName,
			EnterPriseID: e.EnterPriseID,
			EnterpriseName: e.EnterpriseName,
			GID: e.GID,
			HoHttp: e.HoHttp,
			SignVend: e.SignVend,
			UserCode: e.UserCode,
			UserFlag: e.UserFlag,
			UserName: e.UserName,
			isJinher: e.isJinher,
			mac: 			e.mac,
			sex: 			e.sex,
			tokenkey: e.tokenkey,
		}
  }

	// 存入會員資訊
	store.commit('setLoginInfo', {data: userData, pwd: e.pwd})

	console.log('cbFnSyncDeviseInfoToStore ===> Tokenkey =>' + store.state.member.Tokenkey)
}

// (藉由殼 API) 存入登入資訊
function deviseSetLoginInfo(payload) {
  // payload sample: {type: 'set', data: res.data, rememberMe: true/false} , {type: 'reset'}

  // 預設值
  let data = {
		Email: "",
		EngName: "",
		EnterPriseID: "",
		EnterpriseName: "",
		GID: "",
		HoHttp: "",
		SignVend: "",
		UserCode: "",
		UserFlag: 0,
		UserName: "",
		isJinher: false,
		mac: "",
		sex: "",
		tokenkey: "",
  }

  // 存入登入資料
  // if (payload.type === 'set') {
    // data = payload.data
    // // data['MembersName'] = payload.data.Name
    // data['pwd'] = payload.pwd
    // // 存入殼的推播設定
    // // deviseFunction('Do_Register', `{"MembersName":"${data.Name}", "Account":"${data.CardNO}"}`, '')
  // }
  // 寫入要存的 key 設定
  data['_KEY'] = Object.keys(data).join();

  // 執行存入殼裡
  deviseFunction('SetSPS', JSON.stringify(data), '')

// only for test logout
  deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
}

// callback function: 儲存(從殼過來的) 殼版本差異參數
const cbFnSetDeviseVersionIsDiff = function(e) {
  store.commit('SetDeviseVersionIsDiff', e.Obj)
}

// callback function: 儲存(從殼過來的) 簡訊驗證碼
const cbFnSyncDeviseSMSreCode = function(e) {
  // 將簡訊驗證碼存入 state.SMS_Config
  store.commit('setSMSreCode', e)
}
// callback function: LINE Login

const cbFnSyncDeviseGPS = function(e) {
  // console.log('cbFnSyncDeviseGPS ===> e =>', e)	// @@
  // 轉成 google maps api 需要的預設格式
  const gpsData = {gps: {lat: e.Lat, lng: e.Lng} }
  // 將 gps 存入 state.baseInfo
  store.commit('setBaseInfo', gpsData)
}
// callback function: 儲存微管雲系統參數
const cbFbSaveAppSysConfig = function(e) {
	// console.warn('cbFbSaveAppSysConfig: ', e);	// @@
  // 目前只有取 APPSMSDayTimes(每天最多簡訊次數), AppSMSIntervalMin(每封簡訊發送間隔) 這二個值，只需要存這二個值就好
  const data = {
    APPSMSDayTimes: e.APPSMSDayTimes,
    AppSMSIntervalMin: e.AppSMSIntervalMin
  }
  store.commit('setSMS_Config', data)
}

// callback function: 存入每天簡訊發送的扣打紀錄
const cbFnSetSMSSendQuota = function(e) {
  const data = { SMSSendQuota: e }
  store.commit('setSMS_Config', data)
}

// callback function: 存入字體大小
const cbFnSetFontSize = function(e) {
  store.commit('setFontSizeTemp', e)
}

// callback function: 設定-磅秤
// const cbFnSetWt = function(e) {		// 無callback
	// alert('cbFnSetWt: \n'+JSON.stringify(e));	// @@
	// alert('設定成功');	// @@
// }
// callback function: 設定-列印
// const cbFnSetPrn = function(e) {		// 無callback
	// alert('cbFnSetPrn: \n'+JSON.stringify(e));	// @@
	// alert('設定成功');	// @@
// }
// callback function: 列印數值
const cbFnsendToPrn = function(e) {
	// alert('cbFnsendToPrn: \n'+JSON.stringify(e));	// @@
}
// callback function: 磅秤取數值
const cbFnGetweighing = function(v1) {
  store.commit('setGetweighing', v1)
}
// (藉由殼 API) 發送列印資訊
// callback function: 掃瞄QRcode wufu辦活動用
// const cbFnScanQrCode = function(QrCode) {
  // //if (typeof(QrCode) !== 'string') return showAlert('條碼資料錯誤！')
  // if (store.getters.isLogin) {
    // router.replace(QrCode);
  // }else{
    // router.replace({path: '/'})
  // }
// }
const cbFnScanQrSerCode = function(qCode) {
	if (qCode) {
IsHank && alert('ScanQrSerCode: '+qCode);	// @@
		S_Obj.save('M_Scales_GID', qCode);
		store.dispatch('GetSetScaleGid') // 秤重記錄

	} else {
		showAlert('無效的掃瞄碼！')
	}
}



// -- 使用 jsInterFace 跟殼取資料的用法(目前有用到的) --

// GetSPAll => 取全部存入殼的資訊
// JSInterface.CallJsInterFace('GetSPAll', '', '')

// // SetSPS            => 設定多值
// data = { "_KEY": "InvType,InvDes,InvVend", "InvType": "InvType", "InvDes": "InvDes", "InvVend": "InvVend" }
// JSON.stringify(data) // '{"_KEY":"InvType,InvDes,InvVend","InvType":"InvType","InvDes":"InvDes","InvVend":"InvVend"}'
// JSInterface.CallJsInterFace('SetSPS', JSON.stringify(data), '');
// // SetSP             => 將資料存入殼裡
// JSInterface.CallJsInterFace('SetSP', '{"spName":"SMSCode", "spValue":"2345222"}', '')
// // GetSP             => 跟殼取資料
// JSInterface.CallJsInterFace('GetSP', 'SMSCode', '')
// // gps               => 取得 GPS 地理資訊
// JSInterface.CallJsInterFace('gps', '', '')
// // GetAppSysConfig   => 取得殼的系統參數
// JSInterface.CallJsInterFace('GetAppSysConfig', 'AppRegNeedSMSCheck,AppSMSIntervalMin,APPSMSDayTimes', '')
// // AppSMSForgetPW    => 會改會員的帳號密碼，並將新的密碼簡訊發送給會員
// JSInterface.CallJsInterFace('AppSMSForgetPW', '0956241782', '')
// // AppSMSReCode      => 發送驗證簡訊 => callback 會回傳驗證碼 => 也可以用 '0956241782;hello' 自訂驗證碼
// JSInterface.CallJsInterFace('AppSMSReCode', '0956241782', '' );
// // getBrightness     => 取得目前螢幕亮度
// JSInterface.CallJsInterFace('getBrightness', '', '');
// // setBrightness     => 調整螢幕亮度(最亮值 255)
// JSInterface.CallJsInterFace('setBrightness', '255', '');
// // barcode           => 開啟掃描器
// JSInterface.CallJsInterFace('barcode', '', '');
// // openWeburl        => 開啟外部連結
// JSInterface.CallJsInterFace("openWeburl", link, '')
// // tel               => 啟用撥打電話功能
// JSInterface.CallJsInterFace("tel", '0988123123', '')
// // shareTo           => 啟用分享功能
// JSInterface.CallJsInterFace('shareTo', '{"subject":"給目標的抬頭", "body":"給目標的內容", "chooserTitle":"開啟分享時的抬頭"}', '');
// // setIO             => 離開 -> 再進入畫面時，不用再重新 loading
// JSInterface.CallJsInterFace('setIO', '1', '')
// // onFrontEndInited  => 通知殼：前端已經初始化完畢 (殼才會執行 returnJsInterFace )
// JSInterface.CallJsInterFace('onFrontEndInited', '', '')
// // mailto            => 啟用殼的寄信功能
// JSInterface.CallJsInterFace('mailto', '{"mailsubject":"給目標的抬頭", "mailbody":"給目標的內容", "chooserTitle":"開啟分享時的抬頭", "mailreceiver":"sdlong.jeng@gmail.com"}', '')
// // Do_Register       => 推播設定, 存入 App 推播所需要的參數
// JSInterface.CallJsInterFace('Do_Register', '{"MembersName":"Allen", "Account":"0956241782"}', '')
// // clearchache       => 清除網頁 cache
// JSInterface.CallJsInterFace('clearchache', '', '')

/* callback function 集中於此 */
// -- APP 殼的公用 callback 接口 (所有打向殼的 API，都會由殼裡來 call 此 function，回傳資料) --
// 解說： 跟殼溝通的接口

// sample: JSInterface.CallJsInterFace('GetString', 'Account,mac', console.log('hello'))
// => data: {State: "OK", Type: "GetString", Obj: "177f8e5494d251e62ffd68c9dfe903a2"}
// => 殼會執行 window.returnJsInterFace(data, console.log('hello'))
// 所以此 function 是用來接收殼回傳的資訊，並做對應的邏輯處理

// !!重要!! => 由於 returnJsInterFace 被 public 出去，所以任何人都能在 console 執行 returnJsInterFace 來亂 try
// !!重要!! => 所以在裡面的 callback function 務必設定成白名單形式，只接受合法的，不然會造成資安漏洞
const returnJsInterFace = (data, cbFn) => {
  // console.log('returnJsInterFace receive:', data, ", cbFn:", cbFn)

  try {
    // data sample:
    // {State: "OK", Type: "gps", Obj: "{Lat: 37.785835, Lng: -122.406418}"}
    // {State: "OK", Type: "GetAppSysConfig", Obj: {AppRegNeedSMSCheck: "false", AppSMSIntervalMin: "10", APPSMSDayTimes: "3", AppSMSReCode: "4"}}

    // 依據 Type 不同執行各自的邏輯，這樣設計的用意是做白名單，防止被亂 try
    // Policy:
    //   1. !!重要!! 取得資料後要執行的邏輯一律做在 callback function (e.g. 拿取得的資料存入 store, 打 api , 做 xxx 事情)
    //   2. !!重要!! 這裏只做從殼回傳資料的『驗證與轉換』
    //   3. 在 iOS 的 cbFn 是 return fuction, 在 Android 是 return string
    switch(data.Type) {
      // 將多筆資料存入殼裡 (不做 callback)
      case 'SetSPS': break;
      // 設定離開 app 再進入時是否要 reload app (不做 callback)
      case 'setIO': break;
      // 殼的螢幕亮度 (不做 callback)
      case 'setBrightness': break;
      // 將資料存入殼裡 (不做 callback)
      case 'SetSP': break;
      // 將資料(多筆)從殼取出 (因本專案沒用到 GetSPS, 所以不做 callback)
      case 'GetSPS': break;
      // 啟用分享功能 (不做 callback)
      // case 'shareTo': break;
      // 推播設定, 存入 App 推播所需要的參數 (不做 callback)
      // case 'Do_Register': break;
      // 將資料從殼取出
      case 'GetSP':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSetSMSSendQuota') cbFnSetSMSSendQuota(data.Obj)
        if (cbFn === 'cbFnSetFontSize') cbFnSetFontSize(data.Obj)
      break;
      // 將全部存入殼的資料取出
      case 'GetSPAll':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseInfoToStore') cbFnSyncDeviseInfoToStore(data.Obj)
      break;
      // 取系統參數 => 目前只有取 APPSMSDayTimes(每天最多簡訊次數), AppSMSIntervalMin(每封簡訊發送間隔) 這二個值
      case 'GetAppSysConfig':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFbSaveAppSysConfig') cbFbSaveAppSysConfig(data.Obj)
      break
      // 發送簡訊驗證碼
      case 'AppSMSReCode':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseSMSreCode') cbFnSyncDeviseSMSreCode(data.Obj)
      break;
      // 取-磅秤數值
      case 'Getweighing':
        // if (cbFn === 'cbFnGetweighing')
        cbFnGetweighing(data.Obj)
      break;
      // 設定-磅秤
      case 'SetWt':
        // if (cbFn === 'cbFnSetWt')
				cbFnSetWt(data)
      break;
      // 設定-列印
      case 'SetPrn':
        // if (cbFn === 'cbFnSetPrn')
				cbFnSetPrn(data)
      // 設定-列印
      case 'sendToPrn':
        // if (cbFn === 'cbFnsendToPrn')
				cbFnsendToPrn(data)
      break;
      // email 寄信功能
      // case 'mailto':
        // // (只有在 iOS 才有此狀況) 電子郵件未設定，跳出請去設定的訊息
        // // if (data.State === 'error' && data.Obj === '手機電子郵件未設定') showAlert('手機電子郵件未設定, 請參考 <a href="https://support.apple.com/zh-tw/HT201320">電子郵件設定</a>')
        // if (data.State === 'error' && data.Obj === '手機電子郵件未設定') showAlert('手機電子郵件未設定')
      // break;

      // 掃條碼
      case 'barcode':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
				const isOK = data.State !== 'error'
				if (isOK) {
					if (cbFn === 'cbFnScanQrSerCode') {
						cbFnScanQrSerCode(data.Obj);
					// } else if (cbFn === 'cbFnScanWuhuQrCode') {
						// cbFnScanWuhuQrCode(data.Obj);
					}
				}
      break;

      // 存入殼是否需要更新的參數
      case 'versionIsDiff':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (data.State !== 'error' && cbFn === 'cbFnSetDeviseVersionIsDiff') cbFnSetDeviseVersionIsDiff(data)
      break;
      // 其他未設定到的，打 console.warn 來做提醒
      default:
        console.warn('type not defined :', data.Type)
    }
  } catch(error) {
    // 若有任何例外錯誤，要跳 console.error 以利查 bug
		alert('例外錯誤: \n'+JSON.stringify(error));	// @@
    console.error('returnJsInterFace Exception Error: ', error);
  }
}


export { deviseFunction, returnJsInterFace, ensureDeviseSynced, deviseSetLoginInfo, sendDeviseSMSreCode, sendDeviseToPrn }
