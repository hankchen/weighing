import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueCookies from 'vue-cookies'


// 與殼溝通相關的 function
import { deviseFunction, returnJsInterFace } from 'src/libs/deviseHelper.js'


import "./assets/css/materialdesignicons.min.css";
import "./assets/css/_app.scss";
import "./assets/css/app.styl";
import "./assets/css/tailwind.css";

import Buefy from 'buefy'
Vue.use(Buefy)

let LComp1 = null;
Vue.prototype.setLoading = function (bOn){
	if (bOn) {
		LComp1 = this.$buefy.loading.open();
	} else {
		LComp1 && LComp1.close()
	}
}
Vue.prototype.showToast = function (bOK, msg1){
// console.log('showToastshowToast-bOK: ', bOK);	// @@
	msg1 && this.$buefy.toast.open({
		duration: 2000,
		message: msg1+'!',
		type: 'is-'+(bOK ? 'success' : 'danger'),
		position: 'is-top',
		actionText: '',
		queue: true
	})
}

Vue.prototype.showConfirm = function (msg1, callback){
	this.$buefy.dialog.confirm({
		message: msg1,
		cancelText: '取消',
		confirmText: '確定',
		onConfirm: () => callback()
	})
}


// 阻止 vue 在啟動時在 console 跳出生成提示
Vue.config.productionTip = false

// vue instance initial
function initVueInstance() {
  const vue = new Vue({
    store, router,
    render: h => h(App),
  }).$mount('#app')

  // debug 工具: 在 console 上操作 vue instance (只有在測試環境才能開啟)
  if (process.env.NODE_ENV === 'development') window.vue = () => { return vue }
}

function setFontSize() {
  if (typeof(JSInterface) === 'undefined') {
    // console.log('main.js ===> 是網頁')
    store.commit('setFontSizeTemp', VueCookies.get('fontSize'))
  } else {
    // console.log('main.js ===> 是殼')
    deviseFunction('GetSP', 'fontSize', '"cbFnSetFontSize"')
  }
}

// function setMyLoc() {
//   store.dispatch('ensureGetDeviseGPS')
// }

/* 
function popShowConfirm(url) {
  // noCancel就是不顯示[取消]按鈕
  showConfirm("溫馨提示<br>有新的版本可更新喔！", '立即更新', '取消')
    // 點擊確認執行
    .then(() => {
      // 在殼裡面 => 開啟瀏覽器
      if (typeof(JSInterface) !== 'undefined') {
        deviseFunction('openWeburl', url, '')
      // 在瀏覽器 => 瀏覽器新開一個tab
      } else {
        window.open(url, '_blank')
      }
      popShowConfirm(url) // 有這個才可以重複按鈕
    })
    // 點擊取消執行 => 關閉 confirmn 視窗
    .catch(() => store.commit('setCustomModal', { type: '', text: '' }))
}
 */

/*  存入殼相關的設定 */

// 與殼溝通的 callback function 設置
window.returnJsInterFace = (data, cbFn) => returnJsInterFace(data, cbFn)
// 啟用/關閉 手動輸入 QRcode 表格 (public 出去給殼用)
// window._Kakar = { setInputQRCode(status) { store.commit('setInputQRCode', status) } }

// 設定延遲的原因：因為 Vue 的運作太快，導致殼得一些 lib 還沒 loaded 就被執行，在運作時會出錯
// console.log('main.js ===> url =>' + location.href)
// 此console不可以移除
// 在殼裡面固定先清空cookie
if (typeof(JSInterface) !== 'undefined') {
  store.commit('setLogout')
}

// let msg123 = 'main.js 20200806-001 ===> tokenVuex=>' + store.state.member.Tokenkey
  // + '<====> tokenCookie =>' + VueCookies.get('kkTokenkey') + '<='
// console.log(msg123)
// // alert(msg123)

setTimeout(() => {
  // 判斷是否在殼裡面(true/false), 並將判斷值存入 store
  const isDeviseApp = (typeof(JSInterface) === "object")
  store.commit('setIsDeviseApp', isDeviseApp)

  // 殼的設定：離開 -> 再進入畫面時，不用再重新 loading
  deviseFunction('setIO', '1', '')
  // 殼的設定：預設的App url(不設話,跑https://ncw.jh8.tw)
  deviseFunction('SetSP', `{"spName":"HoHttp", "spValue":"https://${store.getters.getBaseUrl}"}`, '')

  // 殼的設定：通知殼前端已經初始化完畢 (殼才會執行 returnJsInterFace )
  deviseFunction('onFrontEndInited', '', '')

  // 取所需的資料
  store.dispatch('fetchInitData')
  setFontSize() // 設定字體大小
  // setMyLoc()
}, 100)

// 殼的設定都完成後，再啟動 Vue instance
//setTimeout(() => initVueInstance(), 400)
setTimeout(() => {

  if (typeof(JSInterface) !== "object") {
    initVueInstance();
  }else{
    var tt= 6;
    var counter = setInterval(() => {
      // 等到殼回應時,清除倒數計時 (GetSPAll callback時),再啟動 Vue instance,AppOS有值,表示有從殼取回
      if (tt === 0 || store.state.baseInfo.AppOS !== '') { clearInterval(counter);initVueInstance(); }
      tt = tt-1;
    }, 100);

  }

}, 400)